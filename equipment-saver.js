(function () {
        var ih = new InventoryHelpers;
        var equipmentHeader = document.documentElement.querySelector("div.uiEquipment > div.heading");
        equipmentHeader.onclick = function(){
                var equipmentList = ih.listEquipment();
                var elJSON = JSON.stringify(equipmentList);
                var reply = prompt("Equipment configuration:",elJSON);
                var replyObject = (reply != "") && JSON.parse(reply);
                if(!ObjectHelpers.equal(replyObject,equipmentList)){
                        ih.restoreEquipment(replyObject);
                }
        };
})()

;

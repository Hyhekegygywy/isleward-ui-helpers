(function () {
        var ih = new InventoryHelpers;
        document.querySelector("div.uiInventory > div.heading > div.heading-text").onclick = function() {
                var input = window.prompt("What to salvage ([from], to, [[maxWorth]]):");
                input = input.split(/[^0-9]+/).map(Number);
                if (input.length==1){
                        input.unshift(1);
                }
                if(input.length==2){
                        input.push(1e9);
                }
                ih.salvageRange(input[0],input[1],input[2]);
        }
})()

;

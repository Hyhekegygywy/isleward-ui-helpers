function Vector(x,y) {
        this.x = x;
        this.y = y;
}

Vector.prototype.clone = function() {
        return new Vector(this.x, this.y);
}

Vector.from = function(v) {
        if(v.hasOwnProperty("x") && v.hasOwnProperty("y")) return new Vector(v.x, v.y);
        if(v.hasOwnProperty(0) && v.hasOwnProperty(1)) return new Vector(v[0], v[1]);
        return null;
}

Vector.prototype.equal = function(v) {
        return this.x==v.x && this.y == v.y;
}

Vector.prototype.add = function(v) {
        this.x += v.x;
        this.y += v.y;
        return this;
}

Vector.prototype.plus = function(v) {
        return this.clone().add(v);
}

Vector.prototype.sub = function(v) {
        this.x -= v.x;
        this.y -= v.y;
        return this;
}

Vector.prototype.minus = function(v) {
        return this.clone().sub(v);
}

Vector.prototype.signum = function() {
        this.x = Math.sign(this.x);
        this.y = Math.sign(this.y);
        return this;
}

Vector.prototype.toString = function() {
        return [this.x,this.y].join(",")
}

function RoutingHelpers(player, grid) {
        this.player = player || window.player;
        this.grid = function(){ return grid || (window.require('js/misc/physics').grid)};
        this.queue = [];
}

RoutingHelpers.prototype.tryStep = function() {
        if (this.queue.length == 0){
                return false;
        } else {
                if(this.player.pather.path.length<18){
                        var next = this.queue.shift();
                        this.player.keyboardMover.addQueue(next.x,next.y);
                        return next;
                } else {
                        return true;
                }
        }
}

RoutingHelpers.prototype.run = function () {
        var success = this.tryStep();
        if(!success){
                return false;
        } else if(success == true) {
                setTimeout(function(x){x.run();}, 1500, this);
                return true;
        } else {
                return this.run();
        }
}

RoutingHelpers.prototype.target = function() {
        var q = this.queue;
        var p = this.player.pather.path;
        if(q.length>0){
                return q[q.length-1];
        }else if(p.length>0){
                return Vector.from(p[p.length-1]);
        }else{
                return Vector.from(player);
        }
}

RoutingHelpers.prototype.addPath = function(p) {
        p.forEach(v => this.queue.push(Vector.from(v)));
}

RoutingHelpers.prototype.addPathRel = function(p) {
        var pos = this.target();
        p.forEach(v => this.queue.push(pos.plus(Vector.from(v))));
}

RoutingHelpers.prototype.addPathSteps = function(p) {
        var pos = this.target();
        p.forEach(v => this.queue.push(pos.add(Vector.from(v)).clone()));
        return this.queue;
}

RoutingHelpers.prototype.routingMap = function (goal, start) {
        var g = Vector.from(goal);
        var seen = {};
        var q = [[g,g]];
        var steps = [
                [1,0], [-1,0], [0,1], [0,-1],
                [-1,-1], [1,1], [-1,1], [1,-1]
        ].map(Vector.from);
        var grid = this.grid();
        var empty=function(v){
                return ! grid[v.x][v.y];
        }
        var cutoff = null;
        if(start) {
                cutoff = Vector.from(start);
        }

        while(q.length>0){
                var step = q.shift();
                var source = step[0];
                var next = step[1];
                if(empty(next)&&!seen.hasOwnProperty(next.toString())){
                        seen[next.toString()] = source;
                        if(cutoff && cutoff.equal(next)){
                                return seen;
                        }
                        steps.forEach(dv => q.push([next,next.plus(dv)]));
                }
        }

        return seen;
}

RoutingHelpers.prototype.pathByMap = function (map, source){
        var p = [];
        var v = Vector.from(source);

        if(map.hasOwnProperty(v.toString())){
                while(!v.equal(map[v.toString()])){
                        v = map[v.toString()];
                        p.push(v);
                }
        }

        return p;
}

RoutingHelpers.prototype.pathBetween = function (source, target){
        return this.pathByMap(this.routingMap(target,source),source)
}

RoutingHelpers.prototype.addDestination = function(target){
        this.addPath(this.pathBetween(this.target(),target))
}

RoutingHelpers.prototype.goTo = function(target){
        this.addDestination(target);
        this.run();
}

RoutingHelpers.prototype.walking = function() {
        return !((this.queue.length == 0) && (this.player.pather.path.length == 0));
}

RoutingHelpers.prototype.showPosition = function () {
        var place = document.documentElement.querySelector("div.boxes div#statPosition");
        if(!place){
                var container = document.documentElement.querySelector("div.boxes");
                var box = document.createElement("div");
                box.setAttribute("class","statBox");
                container.appendChild(box);
                place = document.createElement("div");
                place.setAttribute("id","statPosition");
                place.setAttribute("class","text");
                box.appendChild(place);
        }
        place.textContent = "pos: " + Vector.from(player).toString();
        place.onclick = (function () {
                var target = window.prompt("Enter the new position to go to: ");
                if(target){
                        var target = target.split(/[^0-9]+/).map(Number);
                        this.goTo(target);
                }
        }).bind(this)
}

RoutingHelpers.prototype.autoshowPosition = function () {
        this.showPosition();
        window.require("js/system/events").on("onSceneMove",this.showPosition.bind(this));
}

function InventoryHelpers(player){
        this.player = player || window.player;
}

InventoryHelpers.prototype.salvageRange = function(from,to,price){
        var items = this.player.inventory.items;
        var relevant_items = items.filter(i=>(i.hasOwnProperty("pos")&&i.pos>=(from-1)&&i.pos<to&&i.worth<=(price||1e9)));
        performItemAction = require('ui/templates/inventory/inventory').performItemAction;
        salvage = i => performItemAction(i, 'salvageItem');
        n = relevant_items.length;
        relevant_items.map(salvage);
        return n;
}

ObjectHelpers = {};

ObjectHelpers.uniq = function(l){
        var res = {};
        l.forEach(x=>res[x]=true);
        return Object.keys(res);
}

ObjectHelpers.equal = function (x,y,fail,diff){
        if(fail) {return false;}
        if(typeof x != typeof y) { return false; }
        if(typeof x != "object") { return x == y || (isNaN(x) && isNaN(y)); }
        if(x == null) { return y == null; }
        var ok = true;
        var diffk = null;
        ObjectHelpers.uniq(Array.concat(Object.keys(x),Object.keys(y))).forEach(
                function(k){
                        if(!ObjectHelpers.equal(x[k],y[k],!ok,diff)){
                                ok = false;
                                if(diff&&!diffk){
                                        diffk=k;
                                        console.log(k)
                                }
                        }
                });
        return ok;
}

ObjectHelpers.clone = function(x){
        return JSON.parse(JSON.stringify(x));
}

InventoryHelpers.itemSummary = function(item){
        var subslot = null;
        if(item.hasOwnProperty("runeSlot")){ subslot = item.runeSlot; }
        if(item.hasOwnProperty("equipSlot")&&item.equipSlot.match(/-[0-9]/)){
                subslot = Number(item.equipSlot.replace(/.*-/,""));
        }
        var equipped = (item.hasOwnProperty("eq")&&item.eq)||
                item.hasOwnProperty("runeSlot")||item.hasOwnProperty("equipSlot");
        var clone = ObjectHelpers.clone(item);
        clone.id = null;
        clone.equipSlot = null;
        clone.runeSlot = null;
        clone.eq = null;
        clone.pos = null;
        var exact = ObjectHelpers.clone(clone);
        clone.enchantedStats = null;
        var kind = "equipment";
        if(! item.hasOwnProperty("slot")){
                if(item.type == "consumable"){
                        kind = "consumable";
                }else if (item.hasOwnProperty("ability")&&item.ability){
                        kind = "ability";
                }
        }
        return {
                equipped: equipped,
                kind: kind,
                subslot: subslot,
                exact: exact,
                unaugmented: clone,
        }
}

InventoryHelpers.prototype.listEquipment = function(){
        var items = this.player.inventory.items.map(InventoryHelpers.itemSummary).filter(i=>i.equipped);
        return items;
}

InventoryHelpers.prototype.findEquipment = function(l){
        var usedIds = {};
        var allItems = this.player.inventory.items;
        return l.map(function(i){
                var itemsLeft = allItems.filter(i=>!usedIds.hasOwnProperty(i.id));
                var item =
                        itemsLeft.find(function(j){
                                var summary = InventoryHelpers.itemSummary(j);
                                var diff = false;
                                return (! summary.equipped) &&
                                        ObjectHelpers.equal(summary.exact, i.exact);
                        }) ||
                        itemsLeft.find(function(j){
                                var summary = InventoryHelpers.itemSummary(j);
                                return ObjectHelpers.equal(summary.exact, i.exact);
                        }) ||
                        itemsLeft.find(function(j){
                                var summary = InventoryHelpers.itemSummary(j);
                                return (! summary.equipped) && ObjectHelpers.equal(summary.unaugmented, i.unaugmented);
                        }) ||
                        itemsLeft.find(function(j){
                                var summary = InventoryHelpers.itemSummary(j);
                                return ObjectHelpers.equal(summary.unaugmented, i.unaugmented);
                        }) ||
                        null;
                if(item){ i.id = item.id; usedIds[item.id]=true; };
                return i;
        })
}

InventoryHelpers.prototype.equipItem = function(summary){
        var cpn = "equipment";
        var method = "equip";
        var data = summary.id;
        var slotData = {
                itemId: summary.id,
                slot: summary.subslot
        };

        var client = require('js/system/client');

        if(summary.kind == "consumable"){
                method = "setQuickSlot";
                data = slotData;
        }else if(summary.kind == "ability"){
                cpn = "inventory";
                method = "learnAbility";
                data = slotData;
        }else { }

        client.request({
                cpn: 'player',
                method: 'performAction',
                data: {
                        cpn: cpn,
                        method: method,
                        data: data
                }
        });
}

InventoryHelpers.prototype.restoreEquipment = function(l){
        var lookup = this.findEquipment(l);
        var c = 0;
        var ih=this;
        lookup.forEach(function(x){
                if(x.id){
                        setTimeout(function(){ih.equipItem(x)},c*100);
                        c+=1;
                }
        });
}

;

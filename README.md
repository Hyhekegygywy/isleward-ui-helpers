# isleward-ui-helpers

Here I will collect the Javascript code for injection into Isleward that
implements some UI automation.

`isleward-helpers.js` is a library of functions

`position-display.js` (requires `isleward-helpers.js` to be loaded first)
shows the current coordinates under the experience level, and clicking it asks
for new coordinates — pathfinding is done automatically.

`equipment-saver.js` allows to click the equipment dialog title and copy the
equipment description as JSON. If you paste another description into the dialog
box (and you have those items in the inventory), the script will restore your
build. It tries to handle changes in the augmentation, too, but prefers exact
same item in the same state.

`quest-autocomplete.js` autoclicks ready quests.

`mass-salvage.js` allows to click the inventory title, enter the range of
positions to salvage (and a maximum price), and mass-salvage the items. You
can omit the price (defaults to infinity). You can also omit both starting 
position and price (the only number is interpreted as the final position with
infinite price threshold).

`unwrap-in-marionette.js` and `rewrap-in-marionette.js` are files that help
to debug via Marionette (Firefox automation)
